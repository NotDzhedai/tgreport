from telethon import TelegramClient, functions, types
import asyncio
import argparse
import tkinter as tk
from chan_model import Chan

# Дані, необхідні для автеризації
# Отримати api_id та api_hash можна за посиланням (слідувати інструкції)
# https://my.telegram.org/auth

parser = argparse.ArgumentParser()
parser.add_argument("--api_id", type=int)
parser.add_argument("--api_hash", type=str)


args = parser.parse_args()

root = tk.Tk()
root.geometry("900x500")
root.title("Блокуємо міністерство Правди")
root.resizable(0, 0)

lb = tk.Listbox(root, width=40, height=20)
lb.grid(column=1, row=2, padx=5, pady=5)

tk.Label(text="Копіювати телеграм канали для репортів можна тут: https://t.me/stoprussiachannel").grid(row=0, columnspan=2, pady=10)
tk.Label(text="Введіть телеграм канали").grid(column=0, row=1)
tk.Label(text="Результати").grid(column=1, row=1)


async def report():
    clearListBox()
    links = await prepareLinks()
    await run(links)
    clearTextField()
    
t = tk.Text(root, width=60, height=20)
loop = asyncio.get_event_loop()
b = tk.Button(root, text="Submit", command=lambda: loop.run_until_complete(report()))

t.grid(column=0, row=2, padx=15, pady=5)
b.grid(row=3)

client = TelegramClient('anon', args.api_id, args.api_hash)

# Відсилає репорти на посилання, які є вище
async def run(links):
    async with client:
        for link in links:
            print("Репорт на ", link.get_addr())
            try:
                result = await client(functions.messages.ReportRequest(
                    peer=link.get_addr(),
                    id=link.get_ids(),
                    reason=types.InputReportReasonFake(),
                    message="The channel undermines the integrity of the Ukrainian state. Spreading fake news, misleading people. There are a lot of posts with threats against Ukrainians and Ukrainian soldiers. Block him ASAP!"
                ))
                lb.insert(tk.END, f"Канал {link.get_addr()}, результат: {result}")
                print("Результат: ", result)
            except ValueError:
                lb.insert(tk.END, "Не зміг знайти такий телеграм канал: " + link.get_addr())
                print("Не зміг знайти такий телеграм канал: ", link.get_addr())
            except:
                lb.insert(tk.END, "Сталась якась помилка: " + result)
                print("Сталась якась помилка: ", result)


def clearTextField():
    t.delete('1.0', tk.END)
    
def clearListBox():
    lb.delete(0, tk.END)

async def prepareLinks():
    links = t.get("1.0","end")
    result = []
    if "//t.me/" not in links:
        lb.insert(tk.END, "Неправильний формат посилання")
        clearTextField()
        return result
    for l in links.split("\n"):
        if l == "": continue
        chan = Chan()
        if "https" not in l:
            continue
        partArr = l.split("/")
        chan.set_addr(f"{partArr[2]}/{partArr[3]}")
        if len(partArr) == 4:
            async with client:
                try:
                    msgs = await client.get_messages(chan.get_addr(), limit=10)
                    for m in msgs:
                        chan.set_id(m.id)
                except ValueError or BufferError:
                    print("Невдалося отримати повідомлення від каналу: ", chan.get_addr())
        if len(partArr) == 5:
            chan.set_id(int(partArr[4]))
        result.append(chan)
    return result


if __name__ == "__main__":
    root.mainloop()
    