# Інструкція

1. Встановити python та pip [Посилання](https://phoenixnap.com/kb/how-to-install-python-3-windows)
2. Виконати в cmd (або терміналі)
   ```
   pip install tkinter, telethon
   ```
3. Отримати дані, необхідні для авторизації. Отримати api_id та api_hash можна за посиланням (слідувати інструкції)
   [Посилання](https://my.telegram.org/auth)
4. Виконати в cmd (або терміналі). Замість 123 та "qwerty"
підставте ваші дані.
Важливо:
<ul>
  <li>api_id писати без кавичок</li>
  <li>api_hash писати з кавичками</li>
</ul>

```
  python auto_report_tg.py --api_id=123 --api_hash="qwerty"
```

5. Канали для репортів брати тут: [Посилання](https://t.me/stoprussiachannel)
   ![The San Juan Mountains are beautiful!](/sc1.png)

# !!! Якщо заходите перший раз, то після натискання Submit треба авторизуватись в консолі (просто послідувати інструкції)
