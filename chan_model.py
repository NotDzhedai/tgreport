class Chan():
  _ids = []
  _addr = ""
  
  def __init__(self):
      self._ids = []
      self._addr = ""
      
  def set_addr(self, addr):
    self._addr = addr
    
  def set_id(self, id):
    self._ids.append(id)
    
  def get_ids(self):
    return self._ids
  
  def get_addr(self):
    return self._addr
  
  def __str__(self):
    return f"{self._addr} : {self._ids}"