from telethon import TelegramClient, functions, types, events
import argparse
from utils import insert_time
from chan_model import Chan

parser = argparse.ArgumentParser()
parser.add_argument("--api_id", type=int)
parser.add_argument("--api_hash", type=str)
args = parser.parse_args()

client = TelegramClient('anon', args.api_id, args.api_hash)

async def run(chans):
  for c in chans:
      print(insert_time() + " Репорт на ", c.get_addr())
      try:
          result = await client(functions.messages.ReportRequest(
              peer=c.get_addr(),
              id=c.get_ids(),
              reason=types.InputReportReasonFake(),
              message="The channel undermines the integrity of the Ukrainian state. Spreading fake news, misleading people. There are a lot of posts with threats against Ukrainians and Ukrainian soldiers. Block him ASAP!"
          ))
          print("Результат: ", result)
      except ValueError:
          print("Не зміг знайти такий телеграм канал: ", c.get_addr())
      except:
          print("Сталась якась помилка: ", result)
       
async def getChans(client, message):
  mArr = message.split("\n")
  chanArr = []
  for part in mArr:
    if "https" not in part: continue
    if "//t.me/" in part:
      chan = Chan()
      partArr = part.split("/")
      chan.set_addr(f"{partArr[2]}/{partArr[3]}")
      if len(partArr) == 4:
        try:
          msgs = await client.get_messages(chan.get_addr(), limit=10)
          for m in msgs:
            chan.set_id(m.id)
        except:
          print("Сталась помилка з каналом: ", chan.get_addr())
      if len(partArr) == 5:
        chan.set_id(int(partArr[4]))
      chanArr.append(chan)
  return chanArr

# @client.on(events.NewMessage(chats='Something'))
@client.on(events.NewMessage(chats='stoprussiachannel'))
async def my_event_handler(event):
    chans = await getChans(client, event.raw_text)
    await run(chans)


client.start()
client.run_until_disconnected()